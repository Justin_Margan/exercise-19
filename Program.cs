﻿using System;

namespace exercise_18
{
    class Program
    {
        static void Main(string[] args)
        {
        //Start the program with Clear();
        Console.Clear();
        
        Console.WriteLine($"Question | a (6+7) * (3-2) = {(6 + 7) * (3 - 2) }");
            Console.WriteLine();
        Console.WriteLine($"Question | b (6 * 7) + (3 * 2) = {(6 * 7) + (3 * 2) }");
            Console.WriteLine();
        Console.WriteLine($"Question | c (6 * 7) + 3 * 2 = {(6 * 7) + 3 * 2  }");
            Console.WriteLine();
        Console.WriteLine($"Question | d  (3 * 2) + 6 * 7 = {(3 * 2) + 6 * 7 }"); 
            Console.WriteLine();
        Console.WriteLine($"Question | e  (3 * 2) + 7 * 6 / 2 = {(3 * 2) + 7 * 6 } ");
            Console.WriteLine();
        Console.WriteLine($"Question | f 6 + 7 * 3 - 2 = { 6 + 7 * 3 - 2 }");
            Console.WriteLine();
        Console.WriteLine($"Questin | g 3 * 2 + (3 * 2) = { 3 * 2 + (3 * 2) }");
            Console.WriteLine();
        Console.WriteLine($"Question | h (6 * 7) * 7 + 6 = { (6 *7) * 7 + 6 }");
            Console.WriteLine();
        Console.WriteLine($"Question | i (2 * 2) + 2 * 2 = { (2 * 2) + 2 * 2 }");
            Console.WriteLine();
        Console.WriteLine($"Question | j 3 * 3 + (3 * 3) = { 3 * 3 + (3 * 3) }");
            Console.WriteLine();
        Console. WriteLine($"Question| k (6\xB2 + 7) * 3 + 2 = { (Math.Pow(6,2) + 7) * 3 + 2 }");
            Console.WriteLine();
        Console.WriteLine($"Question | l (3 * 2) + 3\xB2 * 2 = { (3 * 2) + (Math.Pow(3,2)) * 2 }");
            Console.WriteLine();
        Console.WriteLine($"Question | m (6 * (7 + 7)) / 6 = { (6 * (7 + 7)) / 6 }");
            Console.WriteLine();
        Console.WriteLine($"Question | n ((2 + 2) + (2 * 2)) = { (2 + 2) + (2 * 2) }");
            Console.WriteLine();
        Console.WriteLine($"Question | o 4 * 4 + (3\xB2 * 3\xB2) = { 4 * 4 + (Math.Pow(3,2)) * (Math.Pow(3,2)) }");

        //End the program with blank line and instructions
        Console.ResetColor();
        Console.WriteLine();
        Console.WriteLine("Press <Enter> to quit the program");
        Console.ReadKey();
        }
    }
}
